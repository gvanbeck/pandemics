const publish = require('./publish.js');
const fsTools = require('../lib/fs-tools.js');
const path = require('path');

module.exports = (args, options, logger) => {
  let {sources} = args;

  // if no arguments are provided, list all md files in the cwd
  if (!sources.length) {
    sources = fsTools
      .listFiles(
        process.cwd(),
        {
          type: 'file',
          filter: fileName => /\.md$/.test(fileName)
        }
      );
  }

  // loop over files to compile
  const pms = sources.map((sourceFile) => {
    // get full path to file
    const sourcePath = path.resolve(process.cwd(), sourceFile);
    // start compilation
    return publish({
      source: sourcePath,
      target: options.output,
      recipe: options.to,
      format: options.format,
      logger: logger.debug
    });
  });

  // wait for all files to be compiled and display feedback
  Promise.all(pms)
    .catch((err) => {
      logger.error(`*** ${err.message}`);
      process.exit(1);
    })
    .then((result) => {
      process.exit(0);
    });


};
