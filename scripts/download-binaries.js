#!/usr/bin/env node

// this script download external dependencies provided as precompiled binaries

const path = require('path');
const download = require('download');
const decompressTarxz = require('@felipecrs/decompress-tarxz');
const _7z = require('7zip-min');
var fs = require('fs');

const binPath = path.join('assets', 'bin');

const platform = process.env.npm_config_target_platform || process.platform;
const arch = process.env.npm_config_target_arch || process.arch;

// cleanup existing files
// -------------------------------------------------------
let ext = platform == 'win32' ? '.exe' : ''
let pandocTarget = path.join(binPath, 'pandoc', 'pandoc' + ext);
if (fs.existsSync(pandocTarget)) {
  fs.unlinkSync(pandocTarget)
}
let crossrefTarget = path.join(binPath, 'crossref', 'pandoc-crossref' + ext);
if (fs.existsSync(crossrefTarget)) {
  fs.unlinkSync(crossrefTarget)
}

// get pandoc binaries
// -------------------------------------------------------
const pandocVersion = '2.18'
const pandocBaseUrl = `https://github.com/jgm/pandoc/releases/download/${pandocVersion}/`

let pandocFile;
let pandocFilter = file => true;
let pandocMap = file => file;

switch (platform) {
  case 'darwin':
    pandocFile = `pandoc-${pandocVersion}-macOS.zip`;
    pandocFilter = file => path.dirname(file.path) === `pandoc-${pandocVersion}/bin`;
    pandocMap = (file) => {
      file.path = path.basename(file.path);
      return file;
    };
    break;
  case 'linux':
    let pandocArch = arch === 'arm64' ? 'arm64' : 'amd64'
    pandocFile = `pandoc-${pandocVersion}-linux-${pandocArch}.tar.gz`
    pandocFilter = file => path.dirname(file.path) === `pandoc-${pandocVersion}/bin`;
    pandocMap = (file) => {
      file.path = path.basename(file.path);
      return file;
    };
    break;
  case 'win32':
    pandocFile = `pandoc-${pandocVersion}-windows-x86_64.zip`;
    pandocFilter = file => path.extname(file.path) === '.exe';
    pandocMap = (file) => {
      file.path = path.basename(file.path);
      return file;
    };
    break;
  default:
}

download(
  pandocBaseUrl + pandocFile,
  path.join(binPath, 'pandoc'),
  {
    extract: true,
    filter: pandocFilter,
    map: pandocMap
  }
);

// get crossref binaries
// -------------------------------------------------------

const crossrefBaseUrl =
  'https://github.com/lierdakil/pandoc-crossref/releases/download/v0.3.13.0/';

let crossrefFile;
let crossrefFilter = file => true;
let crossrefMap = file => file;

switch (platform) {
  case 'darwin':
    crossrefFile = 'pandoc-crossref-macOS.tar.xz';
    break;
  case 'linux':
    crossrefFile = 'pandoc-crossref-Linux.tar.xz';
    crossrefFilter = file => path.extname(file.path) !== '.1';
    break;
  case 'win32':
    crossrefFile = 'pandoc-crossref-Windows.7z';
    break;
  default:
}

switch (path.extname(crossrefFile)) {
  case '.xz':
    download(
      crossrefBaseUrl + crossrefFile,
      path.join(binPath, 'crossref'),
      {
        extract: true,
        filter: crossrefFilter,
        map: crossrefMap,
        plugins: [decompressTarxz()]
      }
    );
    break;
  case '.7z':
    download(
      crossrefBaseUrl + crossrefFile,
      path.join(binPath,'crossref')
    )
    .on('end',function () {
        setTimeout(function () {
          _7z.unpack(
            path.join(_7z.unpack,'crossref',crossrefFile),
            path.join(_7z.unpack,'crossref'),
            function (err) {
              if(err){
                console.log(err);
              }else{
                fs.unlinkSync(path.join(binPath,'crossref',crossrefFile));
              }
            }
          );
        }, 1000);
    });    break;
}
