const path = require('path');
const os = require('os');

module.exports = {
  RECIPES_PATH: path.join(os.homedir(), '.pandemics'),
  TARGET_PATH: 'pandemics'
};
