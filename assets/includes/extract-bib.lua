function Pandoc(d)
  -- list cited references
  local refs = pandoc.utils.references(d)
  -- remove duplicates
  local refs_unique = {}
  local refs_ids = {}
  -- iterate all citations from the end, to keep the last
  for i = #refs, 1, -1 do
    -- get citekey
    id = refs[i].id
    -- if first encounter with the citekey
    if not refs_ids[id] then
      -- keep track of included citekeys
      refs_ids[id] = true
      -- add the ref to the curated ref list
      table.insert(refs_unique,refs[i])
    end
  end
  -- return minimal doc with only curated ref list
  d.meta.references = refs_unique
  d.meta.bibliography = nil
  d.blocks = {}
  return d
end
