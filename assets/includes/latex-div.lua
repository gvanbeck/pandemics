function Div(el)
  if el.classes[1] == 'pandemics-include-error' then
    local beginEnv = '\\begin{pandemicsIncludeError}'
    local endEnv = '\\end{pandemicsIncludeError}'

    if el.content[1].t == "Para" and el.content[#el.content].t == "Para" then
      table.insert(el.content[1].content, 1, pandoc.RawInline('tex', beginEnv .. "\n"))
      table.insert(el.content[#el.content].content, pandoc.RawInline('tex', "\n" .. endEnv))
    else
      table.insert(el.content, 1, pandoc.RawBlock('tex', beginEnv))
      table.insert(el.content, pandoc.RawBlock('tex', endEnv))
    end
    return el
  end
end
