const assets = require('../lib/assets.js');
const fetch = require('sync-fetch');
const fs = require('fs-extra');
const PandocCommand = require('../lib/PandocCommand.js');
const path = require('path');
const { spawnSync } = require('child_process');

var zoteroBBTurl = "http://127.0.0.1:23119/better-bibtex"

// send API request to zotero BBT using json-rpc
function requestBBT (action, params) {
  var data = {
    jsonrpc: "2.0",
    method: action
  }
  try {
    return fetch (zoteroBBTurl + '/json-rpc', {
    	method: 'post',
    	body: JSON.stringify (data),
    	headers: {'Content-Type': 'application/json'}
    })
    .json ()
    .result;
  } catch (err) {
    throw new Error ('Cannot connect to Zotero')
  }
}

// generate a bib file for the source using zotero libraries
module.exports = function (source, bibfile, logger) {

  // expand path to bibfile
  bibfile = path.join (path.dirname (source), bibfile);

  // prepare call to pandoc
  let command = new PandocCommand ();

  // start using the existing bib, if possible, to carry over previous entries
  if (fs.existsSync (bibfile)) {
    command.pushBibliography (bibfile)
  }
  // add zotero libraries as http pulls
  // get library names
  requestBBT ('user.groups').forEach (group => {
    command.pushBibliography (`${zoteroBBTurl}/export/library?/${group.id}/library.biblatex`)
  });
  // filter to avoid duplicates
  command.pushFilter (assets.file['extract-bib.lua'])

  // set output
  command.pushOption ('to', 'biblatex')
  command.pushOption ('output', bibfile)
  command.pushString (source)

  // run call to pandoc
  var commandArray = command.toArray();
  const bibprocessor = spawnSync(
    commandArray.shift(), commandArray,
    {shell: true}
  )

  logger (`Generate autobib: ${bibfile}`)
  logger (bibprocessor.stderr.toString ())
}
