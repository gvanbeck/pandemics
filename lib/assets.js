const path = require ('path');
const fs = require('fs-extra');
const which = require('which')


const assetPath = path.join (__dirname, '..', 'assets')   ;
var assets = {};

// text filters, headers, etc
// ---------------------------------------------------------------------------
const filePath = path.join (assetPath, 'includes')

assets.file = {
  'error-env.tex': path.join (filePath,'error-env.tex'),
  'latex-div.lua': path.join (filePath,'latex-div.lua'),
  'extract-bib.lua': path.join (filePath,'extract-bib.lua')
}

// add binaries: search if installed as assets, ortherwise look in path
// ---------------------------------------------------------------------------
const vendorPath = path.join (assetPath, 'bin')

function findBin (binPath, binPrefix) {
  // fist look for local deps
  const localBin = fs
    .readdirSync (path.join (vendorPath, binPath))
    .filter (e => e.startsWith(binPrefix));
  if (localBin.length) {
    return path.join (vendorPath, binPath, localBin[0])
  // no local deps, check system wide
  } else {
    try {
      var globalBin = which.sync (binPrefix);
      return globalBin;
  // no joy
    } catch (e) {
      throw new Error(`Coud not find ${binPrefix} on the machine. Please install missing dependencies.`)
    }
  }
}

assets.bin = {
  'pandoc': findBin ('pandoc', 'pandoc'),
  'pandoc-crossref': findBin ('crossref','pandoc-crossref')
}

module.exports = assets;
