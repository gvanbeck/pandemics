const fs = require('fs-extra');
const yamlFront = require('yaml-front-matter');

module.exports = (source) => {
  // if no options provided, check yaml header
  const frontMatter = yamlFront.loadFront(fs.readFileSync(source));

  // clean a little bit
  const content = frontMatter.__content;
  delete frontMatter.__content;

  return {frontMatter, content};
};
